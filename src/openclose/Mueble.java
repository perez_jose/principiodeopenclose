/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package openclose;

/**
 *
 * @author CompuStore
 */
public abstract class Mueble {
private String Tipo;
private String Anchor;
private String Altura;
private String NumeroDePatas;

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    public String getAnchor() {
        return Anchor;
    }

    public void setAnchor(String Anchor) {
        this.Anchor = Anchor;
    }

    public String getAltura() {
        return Altura;
    }

    public void setAltura(String Altura) {
        this.Altura = Altura;
    }

    public String getNumeroDePatas() {
        return NumeroDePatas;
    }

    public void setNumeroDePatas(String NumeroDePatas) {
        this.NumeroDePatas = NumeroDePatas;
    }
    public abstract String Imprimir();
       
    

    
}
